FROM node:10-alpine

RUN echo "@edge http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories

RUN apk update

RUN apk add \
    build-base \
    libtool \
    autoconf \
    automake \
    expat-dev \
    jq \
    openssh \
    python \
    libexecinfo-dev@edge

RUN mkdir /app
WORKDIR /app

COPY package*.json ./

# Add metadata to the image to describe which port the container is listening on at runtime.
EXPOSE 8080

RUN npm install 

COPY . .

RUN npm run build

CMD [ "npm", "start" ]

