import bodyParser from 'body-parser';
import express, { Express } from 'express';
import routes from './routes';

const app = express() as Express;

// Middleware bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Routes
app.use('/', routes);

export default app;
