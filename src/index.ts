import server from './server';

// Defaults
const HOST = process.env.HOST || '0.0.0.0';
const PORT = parseInt(process.env.PORT || '8080', 10);

// Start server
server.listen(PORT, () => {
  console.log(`Node server running on ${HOST}:${PORT}`);
});
