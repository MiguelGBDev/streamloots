export interface ErrorDetails {   
    error?: { name?: string; message: string; stack?: string };
    [key: string]: unknown;
}