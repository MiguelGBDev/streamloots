export default interface Card {
  readonly id: string,
  readonly model_id: string,
  readonly status: 'available' | 'used',
  readonly holder_id: number,
  readonly created_at?: string,
  readonly updated_at?: string;
}
