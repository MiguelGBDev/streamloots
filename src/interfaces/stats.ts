export interface StatsOwnerResponse {
    readonly isOwner: boolean;
    readonly modelId: number;    
    readonly obtainedCards: number;
    readonly usedCardsByAll: number;
}

export interface StatsNonOwnerResponse {
    readonly isOwner: boolean;
    readonly modelId: number;    
    readonly obtainedCards: number;
    readonly usedCardsByHimself: number;
}

export type StatsResponse = StatsOwnerResponse | StatsNonOwnerResponse;