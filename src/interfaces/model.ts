export interface Model {
    readonly name: string,
    readonly type: 'regular' | 'limited',
    readonly published: boolean,
    readonly image: string,
    readonly rarity: 'normal' | 'rare' | 'unique',
    readonly remaining?: number,
    readonly owner_id: string    
}