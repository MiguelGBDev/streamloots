const { Client } = require('pg');
import fakeConversionUserIdToNumber from '../../utils/fakeConversionUserIdToNumber';
import dbConfig from '../dbConfig';

export default {
    fetch: async (modelId: number, userId: string) :Promise<number> => {
        //try {        
            const queryString = 'SELECT count(*) FROM card WHERE model_id = $1 AND holder_id = $2';            

            const integerUserId = fakeConversionUserIdToNumber.fetch(userId);
            const client = new Client(dbConfig);
            client.connect();
                
            const response = await client.query(queryString, [modelId, integerUserId]);            
            return response.rows[0].count;
        //} catch (err) {
            // error manager
        //}
    }
}
