const { Client } = require('pg');
import dbConfig from '../dbConfig';

export default {
    fetch: async (modelId: number) :Promise<number> => {
        //try {
            const queryString = 'SELECT count(*) FROM card WHERE model_id = $1 AND status = "used"';

            const client = new Client(dbConfig);
            client.connect();

            const response = await client.query(queryString, [modelId]);
            return response.rows[0].count;
        //} catch (err) {
            // error manager
        //}
    }
}