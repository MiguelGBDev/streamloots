/** PSEUDOCODE */
/*
Fields required to connect to the database
The best solution is to write this at an .env file, 
so you could manage different database info depending on the environment the API is placed
If the API will run at a kubernetes environment, the best way is save this info in K8s secrets

As I dont have enough time to implement any of this solution I just put data here as the simpliest way
just to achieve connection, as I know this is not a good practice
*/

export default {
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'postgres',
    database: 'streamloots'
}