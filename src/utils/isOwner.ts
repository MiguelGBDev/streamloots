import obtainOwner from './fakeObtainOwner';

export const isOwner = {
    fetch: (modelId: number, userId: string): boolean => {
        const idOwner = obtainOwner.fetch(modelId);
        return userId === idOwner;
    }
}
    