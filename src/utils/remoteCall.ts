/** PSEUDOCODE */
/*
Here should be the call to external third-party services
I should use a library as GOT, fetch, request or axios to make the request (I think GOT is the best of them)

dictionary with platforms URLs (here or at env)
const platforms = {
 platformName1: platform1URL
 platformName2: platform2URL
}

export const remoteCall = {
    fetch: async(platform: string, data: DataResponse, actionService: string): Promise<void> => {
        const importantData = composeObject(data, actionService);
        libraryRemoteCalls.makeCall(platforms[platformName], importantData);
    }
}
*/