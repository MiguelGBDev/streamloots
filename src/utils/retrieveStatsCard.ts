import { StatsOwnerResponse, StatsNonOwnerResponse, StatsResponse} from '../interfaces';
import totalCardsObtainedByModelId from '../db/queries/totalCardsObtainedByModelId';
import userCardsUsedByModelId from '../db/queries/userCardsUsedByModelId';
import totalCardsUsedByModelId from '../db/queries/totalCardsUsedByModelId';

/**
 * This algorithm manage the queries to database in order to obtain the desired statistics.
 * If the user is the owner of a card, the stats will contain total pieces of the card obtained,
 * and the total number of those cards used by everyone.
 * 
 * Otherwise, the stats will be total number of pieces of the card obtained and
 * the number of cards used by himself.
 */
export default {
    fetch: async (modelId: number, userId: string, owner: boolean = true): Promise<StatsResponse> => {
        //total number of cards obtained
        const obtainedCards = await totalCardsObtainedByModelId.fetch(modelId, userId);

        /**
         * We must be sure that the card is published, otherwise queries imply security holes and inconsistency
         * Here a sample of the fake function
         * 
         * if (await cardIspublished(modelId)) {
         *     if (owner) {
         *     ...  
         *     }
         * }
         */                    
        if (owner) {
            //total number of cards used by all the users.            
            const response: StatsOwnerResponse = {
                isOwner: true,
                modelId,
                obtainedCards,
                usedCardsByAll: await totalCardsUsedByModelId.fetch(modelId)
            }

            return response;
        } else {
            //total number of cards used by herself.                       
            const response: StatsNonOwnerResponse = {
                isOwner: false,
                modelId,
                obtainedCards,
                usedCardsByHimself: await userCardsUsedByModelId.fetch(modelId, userId)
            }
            
            return response;
        }        
    }
} 
    