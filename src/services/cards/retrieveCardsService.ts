import { StatsResponse } from '../../interfaces';
import { isOwner } from '../../utils/isOwner';
import retrieveStatsCard from '../../utils/retrieveStatsCard';

/**
 * This method contains the algorithm which manage and compose stats for cards.
 * The algorithm was composed using Promise.All for improve speedup.
 * 
 * The first step is to know if the request's user is the owner or creator of the card:
 * If it is or not, the stats will be a little bit different.
 * 
 * All the queries will be pushed in an array that will throw all the queries in pararell.
 * When all of them was finished, the response will be sent to the user.
 *
 * @param userId - user's identifier
 * @returns user
 */
 export default {
    fetch: async (
        userId: string,
        modelIds: number[]
        ) :Promise<StatsResponse[]> => {            
            const storedRequests = [];

            for (const modelId of modelIds) {                
                if (isOwner.fetch(modelId, userId)) {                    
                    storedRequests.push(retrieveStatsCard.fetch(modelId, userId));
                } else {
                    storedRequests.push(retrieveStatsCard.fetch(modelId, userId, false));
                }                
            }

            const cardsResponse: StatsResponse[] = await Promise.all(storedRequests);            
            
            return cardsResponse;                                        
    }
}
