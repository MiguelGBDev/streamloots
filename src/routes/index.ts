import express from 'express';
import { healthCheck } from './health';
import retrieveCardsController from '../routes/cards/retrieveCardsController';

import { validator } from '../middlewares/validator';
import { authorizator } from '../middlewares/authorizator';

const routes = express.Router();

// Routes
routes.get('/health', healthCheck);
routes.post('/cards', authorizator, validator, retrieveCardsController.fetch);

/** PSEUDOCODE */
//routes.put('./card', authorizator, validator, updateCardController.fetch);
//routes.post('./card', authorizator, validator, createCardController.fetch);
//routes.patch('./cards/publish', authorizator, validator, publishCardsController.fetch);
//routes.patch('./cards/unpublish', authorizator, validator, publishCardsController.fetch);

export default routes;
