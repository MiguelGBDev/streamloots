import { Request, Response } from 'express';
import retrieveCardsService from '../../services/cards/retrieveCardsService';

/**
 * Obtain statistics for the requested cards for the user
 * The method allow to send an array of cards instead just one, 
 * giving more flexibility to the frontend and UX.  
 *
 * @param req - Request
 * @param req.body.userId - user's Id. This value could be obtained from token
 * @param req.body.modelIds - Array of card IDs for retrieve stats and info
 * @param res - Response
 * @returns Void
 */
 export default {
    fetch: async (
        req: Request,
        res: Response
        ) :Promise<void> => {
        try {
            const { userId } = req.body;
            const { modelIds } = req.body;
            
            const response = await retrieveCardsService.fetch(userId, modelIds);

            /** PSEUDOCODE */
            //Here should be the async call to the analytic platforms
            //It doesnt needs await as we dont expect any response from them.
            //removeCall.fetch(platform1, response, 'retrieveCardsService');
            //removeCall.fetch(platform2, response);

            res.status(200).json(response);
        } catch (error) {
            // Here we should have an error manager
            res.status(503).json(error);        
        }
    }
};
