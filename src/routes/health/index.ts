import { Request, Response } from 'express';

/**
 * Endpoint to check that the server is online and running
 *
 * @param _req - Request
 * @param res - Response
 */
export const healthCheck = (_req: Request, res: Response): void => {
  try {
    res.sendStatus(200);
  } catch (error) {
    res.status(error.status).json(error);
  }
};
