import supertest from 'supertest';
import app from '../../server';

const request = supertest(app);

const OK_RESPONSE_CODE = 200;

describe(`GET - /health`, () => {
  it('should return Success OK status', async () => {
    const result = await request.get(`/health`);
    expect(result.status).toBe(OK_RESPONSE_CODE);
  });
});
