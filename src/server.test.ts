import supertest from 'supertest';
import app from './server';

const request = supertest(app);

const INVALIDATE_URL = '/foo';
const STATUS_NOT_FOUND_CODE = 404;

describe('Test invalid route', () => {
  it('should return URL Not Found status code if route is invalid', async () => {
    await request.get(INVALIDATE_URL).expect(STATUS_NOT_FOUND_CODE);
  });
});
