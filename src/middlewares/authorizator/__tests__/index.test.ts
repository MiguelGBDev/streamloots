import { authorizator } from '../';

const VALID_INPUT_OBJECT: any = {
    headers: {
        token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdHJlYW1sb290cy5jb20iL
                CJ1c2VySWQiOiI1YTUwMTU5MzA4ZjVhODAwMTExZGU3NTkiLCJpYXQiOjE1MTYyMzkwMjJ
                9.mj8-t--lfImQGg8HoA_9XOvDlunl3YJoPttkIbOHNMUp`
    }
};

const INVALID_INPUT_OBJECT: any = { headers: {} };

let mockNextFunction: jest.SpyInstance<void>;

const obj = {
    nextFunction: () => {}
}

const res: any = { 
    status: (_code: number) => {},
    send: (_obj: any) => {}
};

describe('Joi validator', () => {
    beforeEach(() => {
        mockNextFunction = jest.spyOn(obj, 'nextFunction');
    });

    afterEach(() => {
        jest.clearAllMocks();
    });
    
    it('when an valid request with headers and token is sent the next method should be called', () => {
      authorizator(VALID_INPUT_OBJECT, res, obj.nextFunction);

      expect(mockNextFunction).toBeCalled();      
    });

    it('when an invalid request with headers and token is sent the next method should be called', () => {        
        authorizator(INVALID_INPUT_OBJECT, res, obj.nextFunction);
  
        expect(mockNextFunction).not.toBeCalled();      
    });
});
