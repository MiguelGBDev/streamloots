import { NextFunction, Request, Response } from 'express';

/**
 * This middleware just ensure that the request include the access token.
 * The sample could be improved with ease for manage access permissions and time-to-live token's property.
 * 
 * @param req 
 * @param res 
 * @param next 
 */
export const authorizator = (
    req: Request,
    res: Response,
    next: NextFunction,
  ): void => {            
    const token = req.headers.token as string;
    
    if(!token) {
        res.status(401);
        res.send({error: 'UNAUTHORIZED', message: 'token not found'});
    } else {        
        /* 
        Autorization step: JWT verification should be implemented here: check user's permissions and if the token is valid
        import jwt from 'jsonwebtoken';
        interface tokenStreamLoots {
            iss: string,
            userId: string,
            iat: Number
        }

        IMPORTANT: Its possible to extract userId using jwt.decode and push it into req object for work with it at controller/service

        try {
            jwt.verify(token);
        } catch (err) {
            res.status(403).send({error: 'FORBIDDEN', message: 'timed out token or restricted access'});
        }
        */

       next();
    }
}