import { NextFunction, Request, Response } from 'express';
import { validate } from './joi-validator';
import { SchemaKeys } from './joi-validator/schemas';

/**
 * This method is a middleware which obtain the input, the route and the request part that we want to check from
 * the express request object, and then call a library which evaluates if the input is correct or not.
 *
 * If the input is correct according to an internal model, the next() method is called and go to the next function.
 * If not, throws a response to the client telling what field is incorrect.
 *
 * @param requestPart - Part of the request that we want to validate
 * @returns The validation function
 */
export const validator = (
    req: Request,
    res: Response,
    next: NextFunction,
): void => {
    const route = req.route.path;
    const { method } = req;
    const reference = (method + route) as SchemaKeys;
    const input: Record<string, unknown> = req.body;

    const validationResponse = validate(input, reference);
    if (!validationResponse.isValid) {
        res.status(400).send({error: 'BAD REQUEST', message: validationResponse.errors});    
    } else {
        next();
    } 
};
