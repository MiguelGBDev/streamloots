import { validate } from '../index';
import { SchemaKeys } from '../schemas';

const CARDS_ROUTE = '/cards';
const HTTP_POST = 'POST';

const VALID_CARDS_OBJECT = {
    userId: '13',
    modelIds: [3, 4, 5]
}

const INVALID_CARDS_OBJECTS = [
    {
        userId: 13,
        modelIds: [3, 4, 5]
    },
    {
      userId: 13,
      modelIds: [3, 4, '5']
    },
    {
      userId: '13',
      modelIds: []
    },
    {
      userId: '13',
      modelIds: ['a']
    },
    {
      userId: '13'      
    },
    {      
      modelIds: [3, 4]
    },
];

describe('Joi validator', () => {
  describe(`${HTTP_POST}${CARDS_ROUTE} route`, () => {
    it('when an valid /cards request is sent the validation should return a valid response', () => {
      const result = validate(
        VALID_CARDS_OBJECT,
        `${HTTP_POST}${CARDS_ROUTE}` as SchemaKeys,
      );

      expect(result.isValid).toBeTruthy();
      expect(result.errors).toBeUndefined();
    });

    it.each(INVALID_CARDS_OBJECTS)(
      'Invalid object iterated should return an error object response',
      (INVALID_OBJECT) => {
        const result = validate(INVALID_OBJECT, `${HTTP_POST}${CARDS_ROUTE}` as SchemaKeys);
        
        expect(result.isValid).toBeFalsy();
        expect(result.errors).toBeDefined();
      },
    );
  });
});
