import Joi from 'joi';

export default Joi.object().keys({
  userId: Joi.string().min(1).max(24).required(),
  modelIds: Joi.array().items(Joi.number()).min(1).required()
});

