import retrieveCardsSchema from './retrieveCardsSchema';

/**
 * This dictionary is implemented to be extended with ease
 * It only needs the HTTP method followed by the route of an endpoint and the schema whose will be compared with.
 */
export const schemas = {
    'POST/cards': retrieveCardsSchema
};
  
export type SchemaKeys = keyof typeof schemas;
  