import Joi from 'joi';
import { schemas, SchemaKeys } from './schemas';
import { ErrorDetails } from '../../../interfaces';

/**
 * This method catch a schema depending of the route of the endpoint passed as parameter and
 * checks if the input matchs the desired schema.
 *
 * This method uses Joi library for validate objects.
 *
 * Return an object with a field isValid which tells about the result of the validation.
 * If the validation is incorrect, also grab the errors from Joi.
 *
 * @param input - input object which will be checked with a schema
 * @param reference - this parameter defines the schema to check the input
 * @returns the result of the validation against schema by Joi
 */
export const validate = (
  input: Record<string, unknown>,
  reference: SchemaKeys,
): { isValid: boolean; errors?: ErrorDetails } => {
  try {
    const schema = schemas[reference];
    Joi.assert(input, schema);
    return { isValid: true };
  } catch (err) {
    return { isValid: false, errors: err.details || err.message };
  }
};
