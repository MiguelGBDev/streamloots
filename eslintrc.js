module.exports = {
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    project: './tsconfig.json',
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.json', '.ts'],
      },
    },
    'import/extensions': [
      'error',
      'ignorePackages',
      { js: 'never', ts: 'never', json: 'never' },
    ],
  },
  plugins: ['@typescript-eslint', 'jsdoc'],
  rules: {
    // these 2 rules are needed to avoid false possitives in TS declarations
    'no-useless-constructor': 'off',
    '@typescript-eslint/no-useless-constructor': 'error',
    'no-console': 'error',
    'import/prefer-default-export': 'off',
    // we should test this rule temporary
    'no-debugger': 1,
    'no-var': 1,
    complexity: ['error', { max: 9 }],
    'max-depth': ['error', { max: 4 }],
    'prefer-const': [
      'error',
      {
        destructuring: 'all',
        ignoreReadBeforeAssign: true,
      },
    ],
    camelcase: [0, { properties: 'never' }],
    'jsdoc/require-jsdoc': [
      'error',
      {
        require: {
          ArrowFunctionExpression: true,
          ClassDeclaration: true,
          ClassExpression: true,
          FunctionDeclaration: true,
          FunctionExpression: true,
          MethodDefinition: true,
        },
      },
    ], // Recommended
    'jsdoc/check-alignment': 1, // Recommended
    'jsdoc/check-examples': 1,
    'jsdoc/check-indentation': 1,
    'jsdoc/check-param-names': 2, // Recommended
    'jsdoc/check-syntax': 1,
    'jsdoc/check-tag-names': 1, // Recommended
    'jsdoc/check-types': 1, // Recommended
    'jsdoc/implements-on-classes': 1, // Recommended
    'jsdoc/newline-after-description': 2, // Recommended
    'jsdoc/no-types': 2,
    'jsdoc/require-description': 2,
    'jsdoc/require-hyphen-before-param-description': 1,
    'jsdoc/require-param': 2, // Recommended
    'jsdoc/require-param-description': 2, // Recommended
    'jsdoc/require-param-name': 2, // Recommended
    'jsdoc/require-returns': 2, // Recommended
    'jsdoc/require-returns-check': 1, // Recommended
    'jsdoc/require-returns-description': 2, // Recommended
    'jsdoc/require-throws': 2, // Recommended
    'jsdoc/valid-types': 1, // Recommended
  },
};
