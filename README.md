# Welcome to MIguel Gomez' Streamloots challenge 👋
![Node Version](https://img.shields.io/badge/node-%3E%3D10.11.0-blue.svg)

This is a API wrote in TypeScript.

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Test](#test-and-coverage)
- [Good practices](#good-practises)


## Introduction

This project is built using the following technologies:

- [TypeScript](https://www.typescriptlang.org/)
- [ESlint](https://eslint.org/) with [Prettier](https://prettier.io/)
- [Joi Validator](https://github.com/sideway/joi)
- [Json Web Token](https://jwt.io/)
- Unit testing with [Jest](https://jestjs.io/) and [Supertest](https://github.com/visionmedia/supertest).
- DataBase management with [Postgres](https://www.postgresql.org/)
- [Docker](https://www.docker.com/)
- [Postgres](https://www.postgresql.org/)


## Prerequisites

1.- Install [Node.js](https://nodejs.org/en/download/) version 10.11.0 or higher

2.- [Git](https://git-scm.com/downloads)

3.- (Optional) For the Development environment, it is recommended the use of [Visual Studio Code](https://code.visualstudio.com/download).

## Proposal
After an exhausted study of the problem, I decided to implement a solution implementing an additional entity non commented at the challenge description: Model. The other entities implied are Users and Cards.

The relationships between those entities are:
- Users could create new model (of cards, of course) becoming owners of them.
- User could have cards, becoming holder of those cards.
- Cards are instancies of a model. 
- Owners could publish/unpublish models, which make cards availables of those models to consume by other users.
- Models only should could be published when all of their attributes are filled.
- Users could spent cards, changing their status to 'used'. The cards don't be removed as those info should be available to made stats charts.
- Onwer could create new cards for limited cards. This attribute is defined in the card's model.

## Features

### REST API
The challenge contains an REST API which contains methods for manage a main resource: cards.

#### * Section 1: HOW TO: RETRIEVE CARD STATS
At this endpoint we retrieve the statistics from cards used and owned by the user who made the call.
We can define if the user is the owner of the cards as we have a token owner and the request send the user id so we could compare both.
The service make always two queries to database, one of them is always the same, the other depends on if the user is owner of the card of not.

#### * Section 2: HOW TO: RETRIEVE CARDS
The proposal is to implement a service which manage cards owned by users. This endpoint should only require an user ID and search at database all the cards 
which have this user ID as holder in TABLE cards (not that the onwer and the holder are not the same relationship, Owner create cards, and holders could spent them)

#### * Section 3: HOW TO: COMPOSE CARDS
The proposal: Create and POST endpoint /cards that could create new models at database (note that you will need to publish this stored model at a second step for start to sell or distribute cards). Once the card is composed you could publish it (changing in this process the published atribute to TRUE). When the model is created and published, the owner should be able to create CARDs from this model, and then distribute or sell them with other flows not included in this challenge.

#### * Section 4: HOW TO: UPDATE CARDS
This proposal is so simple: we only need here an endpoint PUT /card with a parameter modelId, and an user who has owner permissions. Why PUT HTTP verb instead PATCH? Because at the description is suggested many fields should be updated, so, why don't change them together for improve User Experience?

#### * Section 5: HOW TO: PUBLIS/UNPUBLISH CARDS
The model of the cards has an attribute called published that could be changed with an endpoint & query to DB.
All flows should have a check when cards are requested/retrieved that ensure cards are published (specially non-owners).

#### * Section 6: PERSISTENCY: POSTGRES
WHy I decided to use Postgres, well, it is the last database platform I working with, so I though my develpment should be faster as I remember all the stuff better than others.

#### * Section 7: TESTING: JEST
As I wrote in pseudocode, I was not able to implement all the tests I desired. The best approach is create a test suite for all the services, controllers, entities and addtional methods (utls folder) as unitary testing, at least. As I have not enough time, I just put some of them in order to show how I implement them in middlewares folder. Another time, my apologies.

The testing tool choosed is JEST and I used things as Spies, Mocks, and some additional advanced features to make those tests.

#### * Section 8: TYPESCRIPT
All the project was made using TYPESCRIPT as main language (or superset) at an strict mode. All the variables and object have is type or interface defined.

#### * Section 9: DOCKER
I included an standard docker build for this program. For run in docker environment just use docker image & dockre run commands as you can see at documentation linked above.

## Installation

1.- Clone this repo:

- `git clone https://bitbucket.org/MiguelGBDev/streamloots.git`

2.- Install dependencies:

- `npm install`

### Local DataBase:
A dump of the proposal database structure, and some data to play with them are included in the repository (dump.sql). It include three tables for the three proposal entities: User, Card and Model. Some data for all the possible variations are included.

## Usage
- Run server in Development: `npm run start:dev`
- Run from build folder: `npm run start`
- Run deployment: `npm run build`

## Test and coverage

[Jest](https://jestjs.io/docs/en/getting-started) is a JavaScript test runner, that is, a JavaScript library for creating, running, and structuring tests.

- Run tests: `npm run test`

  *Note*: If you want to run a test for a specific file, use this command: `npm run test <file-name>`

- Run tests with Coverage: `npm run test --coverage`
- Run lintr: `npm run lint`
- Run lintr with autofix: `npm run lints:fix`
