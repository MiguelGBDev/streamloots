PGDMP     8    )                y           streamloots #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1) #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16384    streamloots    DATABASE     }   CREATE DATABASE streamloots WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8';
    DROP DATABASE streamloots;
                postgres    false            �            1259    16398    card    TABLE     �   CREATE TABLE public.card (
    id integer NOT NULL,
    model_id integer NOT NULL,
    status character varying(255),
    holder_id integer NOT NULL
);
    DROP TABLE public.card;
       public         heap    postgres    false            �            1259    16396    card_id_seq    SEQUENCE     �   CREATE SEQUENCE public.card_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.card_id_seq;
       public          postgres    false    205            �           0    0    card_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.card_id_seq OWNED BY public.card.id;
          public          postgres    false    204            �            1259    16387    model    TABLE     
  CREATE TABLE public.model (
    name character varying(255),
    type character varying(255),
    published boolean,
    image character varying(255),
    rarity character varying(255),
    remaining bigint,
    id integer NOT NULL,
    owner_id integer NOT NULL
);
    DROP TABLE public.model;
       public         heap    postgres    false            �            1259    16385    model_id_seq    SEQUENCE     �   CREATE SEQUENCE public.model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.model_id_seq;
       public          postgres    false    203            �           0    0    model_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.model_id_seq OWNED BY public.model.id;
          public          postgres    false    202            �            1259    16411    streamloots_user    TABLE     c   CREATE TABLE public.streamloots_user (
    id integer NOT NULL,
    name character varying(255)
);
 $   DROP TABLE public.streamloots_user;
       public         heap    postgres    false            �            1259    16409    streamloots_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.streamloots_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.streamloots_user_id_seq;
       public          postgres    false    207            �           0    0    streamloots_user_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.streamloots_user_id_seq OWNED BY public.streamloots_user.id;
          public          postgres    false    206            F           2604    16401    card id    DEFAULT     b   ALTER TABLE ONLY public.card ALTER COLUMN id SET DEFAULT nextval('public.card_id_seq'::regclass);
 6   ALTER TABLE public.card ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            E           2604    16390    model id    DEFAULT     d   ALTER TABLE ONLY public.model ALTER COLUMN id SET DEFAULT nextval('public.model_id_seq'::regclass);
 7   ALTER TABLE public.model ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            G           2604    16414    streamloots_user id    DEFAULT     z   ALTER TABLE ONLY public.streamloots_user ALTER COLUMN id SET DEFAULT nextval('public.streamloots_user_id_seq'::regclass);
 B   ALTER TABLE public.streamloots_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            �          0    16398    card 
   TABLE DATA           ?   COPY public.card (id, model_id, status, holder_id) FROM stdin;
    public          postgres    false    205   Q       �          0    16387    model 
   TABLE DATA           ^   COPY public.model (name, type, published, image, rarity, remaining, id, owner_id) FROM stdin;
    public          postgres    false    203   �       �          0    16411    streamloots_user 
   TABLE DATA           4   COPY public.streamloots_user (id, name) FROM stdin;
    public          postgres    false    207          �           0    0    card_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.card_id_seq', 6, true);
          public          postgres    false    204            �           0    0    model_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.model_id_seq', 7, true);
          public          postgres    false    202            �           0    0    streamloots_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.streamloots_user_id_seq', 6, true);
          public          postgres    false    206            K           2606    16403    card card_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.card
    ADD CONSTRAINT card_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.card DROP CONSTRAINT card_pkey;
       public            postgres    false    205            I           2606    16395    model model_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.model
    ADD CONSTRAINT model_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.model DROP CONSTRAINT model_pkey;
       public            postgres    false    203            M           2606    16416 &   streamloots_user streamloots_user_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.streamloots_user
    ADD CONSTRAINT streamloots_user_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.streamloots_user DROP CONSTRAINT streamloots_user_pkey;
       public            postgres    false    207            O           2606    16404    card card_model_id_fkey    FK CONSTRAINT     w   ALTER TABLE ONLY public.card
    ADD CONSTRAINT card_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.model(id);
 A   ALTER TABLE ONLY public.card DROP CONSTRAINT card_model_id_fkey;
       public          postgres    false    205    203    2889            P           2606    16422    card foreignkeyuser    FK CONSTRAINT        ALTER TABLE ONLY public.card
    ADD CONSTRAINT foreignkeyuser FOREIGN KEY (holder_id) REFERENCES public.streamloots_user(id);
 =   ALTER TABLE ONLY public.card DROP CONSTRAINT foreignkeyuser;
       public          postgres    false    2893    205    207            N           2606    16417    model foreingkeyuser    FK CONSTRAINT        ALTER TABLE ONLY public.model
    ADD CONSTRAINT foreingkeyuser FOREIGN KEY (owner_id) REFERENCES public.streamloots_user(id);
 >   ALTER TABLE ONLY public.model DROP CONSTRAINT foreingkeyuser;
       public          postgres    false    2893    207    203            �   ;   x�3�4�L,K��IL�I�4�2�K�SS�LcNS$)S.Ns��(ZM��`ZM�b���� J�      �   i   x�]��
@@��?#��%��\�m�ɾ?�(��'���@y��)xq��o�n*.`�`aIR,�����%����2Eu�09jTw�o��fS����Ѡ�1#��1�      �      x�3�,-N-2�2�F\f`ژ+F��� x T     